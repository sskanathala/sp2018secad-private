import java.lang.*;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;



public class EchoServer {
	
    static ThreadList threadlist = new ThreadList();
	public static void main(String[] args) throws IOException {
        
	
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        try {
            ServerSocket serverSocket =
                new ServerSocket(Integer.parseInt(args[0]));
            System.out.println("EchoServer is running at port " + Integer.parseInt(args[0]));
			while(true){    
				Socket clientSocket = serverSocket.accept(); 
				System.out.println("A client is connected ");
				EchoServerThread newthread = new EchoServerThread(threadlist,clientSocket);
				//threadlist.addThread(newthread);
				newthread.start();
		    }
        }catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
	
}

class EchoServerThread extends Thread{
	private Socket clientSocket = null;
	private ThreadList threadlist = null;
	private PrintWriter out= null;
	
	private ThreadList thread = new ThreadList();
	
	private HashMap<String,String> users = thread.getUsers();
	
	public EchoServerThread(Socket socket){
		clientSocket = socket;
	}
	
	public EchoServerThread(ThreadList threadlist, Socket socket){
		this.threadlist = threadlist;
		clientSocket = socket;
	}
	
	public void run(){
		System.out.println("A new thread for client is running. Number of connected clients:" +threadlist.getNumberofThreads());
		
		try{
			/*PrintWriter*/out = new PrintWriter(clientSocket.getOutputStream(), true);                   
		    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	    
		    String inputLine;
		    
		    out.println("You have not loggin. to login, use <JOIN>username:password");
		    
		    while(!(inputLine = in.readLine()).contains("JOIN"))
			{
		    	sendMessage("Wrong Usage:use <JOIN>username:password");
		    }
		    
		    out.println("input line" + inputLine);
		    
		    int conn = 0;
		    
		    String[] arr = login(inputLine);
		    
		    while((inputLine)!=null && conn == 0)
			{
				
		    	String username = arr[0];
		    	
		    	String password = arr[1];
		    	
		    	for(Map.Entry<String,String> entry : users.entrySet())
				{
					
					if((entry.getKey()).equals(username) && password.equals(entry.getValue()))
					{
						out.println("Welcome");
						
						System.out.println("This is " + username);
						
						conn = 1;
						
						threadlist.addThread(this,username);
						System.out.println("Connected Users: " + threadlist.getNumberofThreads()); 
						
						break;
					}
		    	
		    	}
		    
		    
		    	if(conn == 0)
				{
		    		out.println("Invalid Username and password");
		    		
		    		arr = login(in.readLine());
		    		
		    	}
		    
		    }
		    
		    
		    
		    
		    while ((inputLine = in.readLine()) != null) 
			{
		    
		        System.out.println("received from client: " + inputLine);
		        
		        System.out.println("Echo back");
		        
		       // out.println(inputLine);
		       
		       
		       
		       if(inputLine.equals("<LIST>"))
			   {
					ArrayList<String> list = threadlist.getUserList();
		       	
			       	for(int i=0;i<list.size();i++)
					{
			       	
			       		sendMessage(list.get(i));
			       	}
			       
		       }
				else if(inputLine.contains("<PRIV>"))
				{
					String privname;
		   		
					String message;
		   		
					String[] mess = inputLine.split(">");
		   		
					String[] priv = (mess[1].toString()).split(":");
					
					privname = priv[0];
					
					message = priv[1];
					
					if(threadlist.sendMessagePrivate(privname,message) == false){
					
						sendMessage("User doesnot exist");
					}
					
				}  
				else if(inputLine.equals("<EXIT>")){
					threadlist.sendToAll("To All: A client exists, the number of connected client:" + (threadlist.getNumberofThreads()-1));
					threadlist.removeThread(this);
					clientSocket.close();
				}
				else
					sendMessage("Bad Command. Please try either <LIST> | <CHAT>all:message | <PRIV>username:message ");
				
			}	
			
		}
		catch(IOException ioe)
		{
			System.out.println("Exception in thread: "+ ioe.getMessage());
		}
		
	}
	
	
		public void sendMessage(String message)
		{
			if(out!=null)
			out.println(message);	
		}
		
		
		public String[] login(String user){
			
			
			
			String username;
			
			String password;
			
			String[] tokens = user.split(">");
			
			String[] arr = (tokens[1].toString()).split(":");
			
			username = arr[0];
			
			password = arr[1];
				

			
			return new String[]{ username, password};
		}
		

}

class ThreadList{
	//private ArrayList<EchoServerThread> threadlist = new ArrayList<EchoServerThread>();//store the list of threads in this variable	
	
	HashMap<String,String> users = new HashMap<String,String>(){
		{
			put("ShivaSai","ssk92");
			
			put("anil","mar3");
			
			put("Suresh","res16");
			
			put("venu","sai12");
			
			
		}
	
	};
	
	private static HashMap<EchoServerThread, String> threadlist = new HashMap<EchoServerThread, String>();
	
	public ThreadList(){		
	}
	
	public synchronized int getNumberofThreads(){
	//public synchronized int getNumberofThreads(){
	//return the number of current threads
	return threadlist.size();
	}
	
	public synchronized void addThread(EchoServerThread newthread, String username){
	//add the newthread object to the threadlist
	threadlist.put(newthread,username);	
	}
	
	public synchronized void removeThread(EchoServerThread thread){
	//remove the given thread from the threadlist	
	threadlist.remove(thread);	
	}
	
	public void sendToAll(String message)
	{
		//ask each thread in the threadlist to send the given message to its client		
		/*Iterator<EchoServerThread>threadlistIterator = threadlist.iterator();
		while(threadlistIterator.hasNext()){
			EchoServerThread thread = threadlistIterator.next();
			thread.send(message);
		}*/
		
		
		for(EchoServerThread thread : threadlist.keySet()){
			
				thread.sendMessage(message);
		}
		
		
	}
	
	public boolean sendMessagePrivate(String username, String message)
	{
		if(threadlist.containsValue(username)){
			for(EchoServerThread thread : threadlist.keySet()){
			
			
				if((threadlist.get(thread)).equals(username))
				
					thread.sendMessage("<PRIV> "+username+":"+message);
			
			}
			
			return true;
			
		}
	
		else
		
			return false;
	}
	
	public HashMap<String,String> getUsers()
	{
	
		return users;
	}
	
	public ArrayList<String> getUserList()
	{
		ArrayList<String> arr = new ArrayList<String>();
		
		for(String str : threadlist.values()){
			arr.add(str);
		}
		
		return arr;
	
	}
}

