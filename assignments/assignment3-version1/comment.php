<!DOCTYPE html>
<html>
<head>
<style>
 body{
	background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%); /* Standard syntax (must be last) */
     }
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
tr:nth-child(odd){
    background-color: #f1f1c1;	
}
</style>
</head>
<body>
<div style="float:right;"><h3><a href ="form.php">Admin</a> | <a href ="index.php">Home</a></h3></div>
<h2> Welcome To Blog by "Shiva Sai Kanathala"</h2>
<?php
	session_start();
	include 'header.php';	
	require 'mysql.php';
	
	$postid = $_REQUEST['postid'];
	if(!isset($postid)){
		echo "Bad Request";
		die();
	}
	function handle_new_comment($postid){
		$title = $_POST['title'];
		$content = $_POST['content'];
		$commenter = $_POST['commenter'];
		$nocsrftoken = $_POST["nocsrftoken"];
		$sessionnocsrftoken = $_SESSION["nocsrftoken"];

		if(isset($title) and isset($content)){//if the new comment form is submitted
		//prevent CSRF
			if(!isset($nocsrftoken) or ($nocsrftoken!=$sessionnocsrftoken)){
				echo "Cross-site request forgery is detected!";
				die();
			}
			if(new_comment($postid,$title,$content,$commenter))
				echo "New comment added";
			else
				echo"Cannot add the comment";
		}
	}
	handle_new_comment($postid);//function defined above to handle if the request is a new comment
	display_singlepost($postid);//function defined in mysql.php
	display_comments($postid);//function defined in mysql.php
	$rand = bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;
	?>
	<form action="comment.php?postid=<?php echo $postid; ?>" method="POST" class="form login">
		<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
		Your name: <input type="text" name="commenter" /><br>
		Title: <input type="text" name="title" required /><br>
		Content: <textarea name="content" required cols="70" rows="10"></textarea><br>
		<button class="button" type="submit">Post new comment</button>
	</form>	
</body>
</html>	
