<html>

<?php
  require 'security.php';
  include 'header.php';
  require 'mysql.php';
  
   session_start();
  $owner = $_SESSION["username"];

function handle_new_post($owner){

  
  $title = $_POST['title']; //storing username from session
  $text = $_POST['text'];

    if(isset($title) and isset($text)){
     if(new_post($title, $text, $owner))
      echo "<h3>New post added Successfully!!</h3>";
      else
       echo "<h3>Cannot add the post</h3>";	
}
}

handle_new_post($owner);

?>
<head>
<style>
 body{
	background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%); /* Standard syntax (must be last) */
     }
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
tr:nth-child(odd){
    background-color: #f1f1c1;	
}
</style>
</head>
<body>
<h3><a href ="index.php">Home</a> | <a href ="admin.php">Admin </a> | <a href="logout.php">Logout</a></h3> <br>
<h1> Administration of <?php echo htmlspecialchars($_SESSION["username"]);?> blog</h1>

<h1>Create a new post!!</h1>
<form method="post" action = "newpost.php" class = "New post">

	<table>
		
		<tr>
			<td><label for="title">Title</label></td>
			<td><input name="title" id="title" /></td>
		</tr>
		<tr>
			<td><label for="text">Text</label></td>
			<td><textarea name="text" id="text" cols = "70" rows="10"></textarea></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Add" /></td>
		</tr>
	</table>
</form>





</body>
</html>

