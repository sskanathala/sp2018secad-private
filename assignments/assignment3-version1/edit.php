<!DOCTYPE html>
<html>
<?php

require "security.php";
include "header.php";
require "mysql.php";
session_start();
$postid = $_GET['postid'];

$sql = "select * from posts where id = '$postid';";

$result = $mysqli->query($sql);
$row=$result->fetch_assoc();

$username = $row['owner'];

function handle_edit_post($postid, $username){
$title = $_POST['title'];
$content = $_POST['content'];


if($username!=$_SESSION["username"]){  
         //we could disable this code, because we are using username stored in session only.
      echo " Cannot edit post, using Wrong user name....!!";
      die();
}
if(isset($title) and isset($content)){
   if (edit_post($title, $content, $postid))

	echo "Successfully edited post";
	else
	echo "Cannot edit post";
}
}

handle_edit_post($postid, $username);

?>
<head>
<style>
 body{
	background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%); /* Standard syntax (must be last) */
     }
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
tr:nth-child(odd){
    background-color: #f1f1c1;	
}
</style>
</head>

<body>

<body>
 <a href ="index.php">Home</a> | <a href ="admin.php">Admin</a>  | <a href="logout.php">Logout</a> <br>

<h2> Administration of blog by "<?php echo htmlspecialchars($_SESSION['username']);?>" </h2>


<form method="post" action = "edit.php?postid=<?php echo $postid?>" class = "edit post">

									
    <table>
        <tr>
            <td><label for="title">Title</label></td>
            <td><input name="title" id="title" value="<?php echo $row['title']?>"/></td>
        </tr>
        <tr>
            <td><label for="content">Content</label></td>
            <td><textarea name="content" cols = "70" rows="10" id="content"><?php echo $row['content']?> </textarea></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Update"/></td>
        </tr>
    </table>
</form>

</body>

</html>


