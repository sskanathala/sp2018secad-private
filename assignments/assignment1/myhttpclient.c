/* include libraries */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main (int argc, char *argv[])
{
	
	
	char *url=argv[1];
	int port = 80;
	char host[1000];
	char path[1000];
	char servername[1000];
	char filename[100];// get the filename from URL 
	
	
	
	if(argv[1]){
		
		printf("This is a simple TCP Client application Developed by shiva sai for lab2 in Secure Application Development -Spring 2018\n");
			
		sscanf(url,"http://%[^/]/%s\n", host,path);
			
			if(argv[1] &&(strnlen(argv[1],sizeof(servername)+1) <= sizeof(servername)))
			{
				
				strncpy(servername, host, sizeof(servername));
				
			}else{
				
				printf("Empty or too long string\r\n");	
				
				exit(0);
				
			}
		
		printf("Host : %s \r\n",host);
	
		printf("Path: %s\r\n",path);
	
	}
	else{
		
		printf("usage: %s <input url>\n",argv[0]);
		
		exit(0);
	}
	
	struct hostent *ghbn=gethostbyname(host);//change the domain name
	//printf("IP ADDRESS->%s\n",inet_ntoa(*(struct in_addr *)ghbn->h_name) );
	
	if (argc!=2){
		
		printf("Usage: %s <servername> <port>\r\n",argv[0]);
		
		exit(0);
		
	}
	
	//printf("Servername=%s,port=%s\n",argv[1],argv[2]);
	
	
	
	printf("Servername= %s, port=%d\n",servername,port);
	
	int sockfd=socket(AF_INET,SOCK_STREAM,0);
	
	if(sockfd<0)
	{
		
		perror("cannot create socket");
		
		exit(1);
		
	}
	
	struct hostent *server_he;
	
	if((server_he=gethostbyname(servername))==NULL)
	{
		perror("Cannot resolve the hostname");
		
		exit(2);
	}
	
	struct sockaddr_in serveraddr;
	
	bzero((char *) &serveraddr,sizeof (serveraddr));
	
	serveraddr.sin_family=AF_INET;
	
	bcopy((char *) server_he->h_addr,

	(char *) &serveraddr.sin_addr.s_addr,
	server_he->h_length);
	
	serveraddr.sin_port=htons(port);
	
	int connected=connect(sockfd,(struct sockaddr *)
	& serveraddr,sizeof(serveraddr));
	
	//to get the filename 
	char *str = "index.html" ;
	
	const char ch = '/';
	
	char *ret; 
	
	//char *ret2;
	
	//ret2 = strrchr(path,' ');
	//regular expressionn
	
	if(strrchr(path,'.')== NULL){
		
		//if(filename == str){
			strncpy(filename, str, sizeof(filename));
			//printf("Filename:%s\n",filename);
		//}else{
		//	printf("invalid filename\r\n");
		//	exit(0);
		//}	
		
	}
	else
	{
		
	ret = strrchr(path, ch);
		//printf("Filename: %s\n", str);
	
	
	strncpy(filename,ret+1, sizeof(filename));
	
	
		
	}
	
	
	
	if(connected<0)
	{
		perror("cannot connect to server\n");
		
		exit(3);
		
	}
	else
		printf("Connected to the Host %s (%s) to retrieve the document: %s\r\n",servername,inet_ntoa(*(struct in_addr *)ghbn->h_name),path);
	
	
	char *msg="this is just a test message from client";
	
	int byte_sent;//=send (sockfd,msg,strlen(msg),0);
	
	const int BUFFER_SIZE = 1024*1024; // 1 Megabyte
	
	char buffer[BUFFER_SIZE]; 
	
	//char buffer[1024];
	//printf("enter your message:");
	
	//bzero(buffer,1024);
 	bzero(buffer, BUFFER_SIZE);
 	
 	
 	
	sprintf(buffer, "GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n",path, servername);
	
	
	//sprintf(buffer, "GET /PhuPhung/sp2018secad-lab2.html HTTP/1.0\r\nHost:academic.udayton.edu\r\n\r\n", servername);
	//sprintf(buffer, "GET /guide/bgnet/pdf/bgnet_USLetter.pdf HTTP/1.0\r\nHost:beej.us \r\n\r\n", servername);
	//fgets(buffer,1024,stdin);
	
	byte_sent=send(sockfd,buffer,strlen(buffer),0);
	

	bzero(buffer, BUFFER_SIZE);
	
	
	//bzero(buffer,1024);
	//int byte_received=recv(sockfd,buffer,1024,0);
	
	/*receive the first package up to the size of buffer, this data package should contain the HTTP headers, so 1 Mbyte buffer is long enough! */
	
	int bytes_received = recv(sockfd,buffer, BUFFER_SIZE,0);
	 
	

	if(bytes_received<0)
	{
		
		perror("error in reading");
		
		exit(4);
	}
	//printf("received from server %s\r\n",buffer);
	
	int count = 1;
	
	int http_code;	////handle the HTTP response code, if it is 200, write to file 
	
	int total_bytes_received = 0;
	

	
	
	sscanf(buffer,"HTTP/1.%*[01] %d \n",&http_code);
	
	if(http_code == 200){
		
		
		
		printf("success\r\n");
		
		printf("Filename: %s\n", filename);
		
		printf("Receiving file\r\n");
 	
 		printf("saving to file %s\r\n",filename);
 		
 		
		printf("Filename:%s\r\n",filename);
		
		FILE *fileptr=fopen(filename,"w+"); 
		
		///* you need to get the end of the HTTP headers (hint: use search string example in Slide 11*/ 
		
		char *end_of_HTTP_header = strstr(buffer, "\r\n\r\n");//... /* calculate the number of bytes left in the first message: "HTTPheader \r\n\ndata..data.."*/ 
		
		
		
		//int data_length_after_HTTP_header = strlen(end_of_HTTP_header);//... 
				
		//int length_http_header = bytes_received - data_length_after_HTTP_header+4;
		
		//int data_length_after_HTTP_header = bytes_received - length_http_header;
		
		//int buffer_data = strlen(buffer);
		
		int header_size = (end_of_HTTP_header - buffer);
		
		int data_length = bytes_received - (header_size+4);
		
		//printf("data length:%d",data_length);
		
		//printf("length of http header:%d\r\n",length_http_header);
		
		printf("First package: bytes_received = %d",bytes_received);
		
		printf(" header_length = %d\r\n",header_size);
		
		printf("Data_length after HTTP_header = %d\r\n",data_length);
		
		
		total_bytes_received = bytes_received;
		
		fwrite(end_of_HTTP_header+4, data_length,1, fileptr); 
		///* loop to write the rest of the data to file"*/ 
		
		
		
		while((bytes_received=recv(sockfd,buffer, BUFFER_SIZE,0))!=0) 
		{	
			count++;
			
			fwrite(buffer, bytes_received,1, fileptr);
			
			total_bytes_received = total_bytes_received + bytes_received;
	
			
		}
		
		
		printf("times_to_receive (buffer size: %d) = %d\r\n", BUFFER_SIZE, count);
		
		printf("Bytes_received_total =  %d\r\n",total_bytes_received);
		
		
		
	}
		else if(http_code == 404){
			
			printf("404 requested document not found\r\n");
			
		}else if(http_code == 301){
			
			printf("301 requested object moved, new loation!\r\n");
			
		}else if(http_code == 400){
			
			printf("400 Bad Request\r\n");
			
		}else if(http_code == 505){
			
			printf("505 HTTP version not supported\r\n");
			
		}
		
	else{
		printf("Invalid URL\r\n");
		exit(0);	
		
	}	
	
	close(sockfd);
}
