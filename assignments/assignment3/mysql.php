<?php
  echo "->mysql.php";
 //for debug only; delete this line after the complete development
  //Security principle: Never use the root database account in the web application
  $mysqli = new mysqli('localhost', 'shivasai' /*Database username*/,
                                    'Secure*123'  /*Database password*/, 
                                    'shivasai' /*Database name*/);

//echo"one";
	
  if ($mysqli->connect_error) {
      die('Connect Error (' . $mysqli->connect_errno . ') '
              . $mysqli->connect_error);
  }
 
	//echo"two";
  function mysql_checklogin_secure($username, $password) {
    global $mysqli;
   

    $prepared_sql = "SELECT * FROM users WHERE username= ?" 
    ." and password=password(?);";

    if(!$stmt = $mysqli->prepare($prepared_sql))echo "Prepared Statement Error";

    $stmt->bind_param("ss", $username,$password);

    if(!$stmt->execute()) echo "Execute Error";

    if(!$stmt->store_result()) echo "store_result Error";

    if($stmt->num_rows == 1) return TRUE;

    return FALSE;

  }


		

  function mysql_checklogin_usersecure($username,$password){
   global $mysqli;
   
    $prepared_sql = "SELECT * FROM regularuser WHERE approved = 1 and enable =1 and username= ?" 
    ." and password=password(?);";
	 //$result = $mysqli->query($prepared_sql);

    if(!$stmt = $mysqli->prepare($prepared_sql))echo "Prepared Statement Error";


    $stmt->bind_param("ss", $username,$password);

    if(!$stmt->execute()) echo "Execute Error";

    if(!$stmt->store_result()) echo "store_result Error";

    if($stmt->num_rows == 1) return TRUE;

    return FALSE;
	/*  if($result->num_rows == 0){
		echo "No users registered";
	}*/
	
  }


  function mysql_change_users_password($username, $newpassword) { //implemented secure function for securing the application from SQL injection attacks
    global $mysqli;
    $prepared_sql = "UPDATE users SET password=password(?) WHERE username=?;";
  
  if(!$stmt = $mysqli->prepare($prepared_sql))
   echo "Prepared Statement Error";
   $stmt->bind_param("ss",$newpassword, $username);
  if(!$stmt->execute()) {
   echo "Execute Error: UPDATE users SET password=password(?) WHERE username=?;";
   return FALSE;
}

   return TRUE;

  }

 function mysql_change_regularusers_password($username, $newpassword) { //implemented secure function for securing the application from SQL injection attacks
    global $mysqli;
    $prepared_sql = "UPDATE regularuser SET password=password(?) WHERE username=?;";
  
  if(!$stmt = $mysqli->prepare($prepared_sql))
   echo "Prepared Statement Error";
   $stmt->bind_param("ss",$newpassword, $username);
  if(!$stmt->execute()) {
   echo "Execute Error: UPDATE users SET password=password(?) WHERE username=?;";
   return FALSE;
}

   return TRUE;

  }

 function show_posts() {
  //debug: echo "->show_posts: retreive the posts from database and display<br>";    
	
	

    global $mysqli;
   
	//echo "from posts ";

    $sql = "SELECT * FROM posts";
	
    $result = $mysqli->query($sql);
   
    if($result->num_rows > 0){

		while($row = $result->fetch_assoc()){
		
			$postid = $row["id"];
			echo "<h3>Post " . $postid . " - " . $row["title"]. " 		</h3>";
			echo $row["text"]."<br>";
			//echo " hi";	
			echo "<a href='comment.php?postid=$postid'>";
			$sql = "SELECT * FROM comments WHERE postid='$postid';";
			$comments = $mysqli->query($sql);
			if($comments->num_rows > 0){
				echo $comments->num_rows . "comments </a>";	
			}else{
				echo "Post your first comment </a>";
			}
	 
        }
	
    }
	else
	{
		echo "No post in this blog yet <br>";
	}
}

//echo"four";
function new_post($title, $text, $owner){
	
	global $mysqli;
	echo "Shiva from New_post";
	if(isset($title) and isset($text) and isset($owner)){

	$prepared_sql = "INSERT into posts (title,text,owner) VALUES (?,?,?);";
	if(!$stmt = $mysqli->prepare($prepared_sql))
		echo "Prepared Statement Error";

    $stmt->bind_param("sss", htmlspecialchars($title),htmlspecialchars($text),htmlspecialchars($owner));

	echo "Hello shivasai";

    if(!$stmt->execute()) {echo "Execute Error"; return FALSE;}
	
	return TRUE;
	}
	else{
		echo"failed-----------------";
	}
}




function display_singlepost($postid){
	 global $mysqli;
   echo "post for id = $postid"; 
    $prepared_sql = "SELECT * FROM posts WHERE id=?";

}

function display_comments($postid){
	global $mysqli;
	echo "Comment for postid= $postid <br>";
	$prepared_sql = "select title, content from comments where postid=?;";
	if(!$stmt = $mysqli->prepare($prepared_sql))
	echo"prepared Statement error";
	
   	 $stmt->bind_param("i", $postid);
	if(!$stmt->execute()) echo"execution failed";
	$title = NULL;
	$content = NULL;
	if(!$stmt->bind_result($title,$content)) echo"BInding failed";
	$num_rows = 0;
	while($stmt->fetch()){
	echo"<br> comment title:" . htmlentities($title) . "<br>";
	echo htmlentities($content) . "</br>";
	$num_rows++; 
	}
	if($num_rows==0) echo "No comment for this post.Please post your comment";
}

function new_comment($postid,$title,$content,$commenter){
	global $mysqli;
   
    $prepared_sql = "INSERT into comments(title, content,commenter, postid) VALUES (?,?,?,?);";

    if(!$stmt = $mysqli->prepare($prepared_sql))
		echo "Prepared Statement Error";

    $stmt->bind_param("sssi", htmlspecialchars($title),htmlspecialchars($content),htmlspecialchars($commenter),htmlspecialchars($postid));

    if(!$stmt->execute()) {echo "Execute Error"; return FALSE;}
	
	return TRUE;	

}


//start edit post
function edit_post($title, $content, $postid){

    global $mysqli;
    $prepared_sql = "UPDATE posts SET title=?, text=?  WHERE id=?;";
  
  if(!$stmt = $mysqli->prepare($prepared_sql))
   echo "Prepared Statement Error";
   $stmt->bind_param("ssi",htmlspecialchars($title), htmlspecialchars($content), $postid);
  if(!$stmt->execute()) {
   echo "Execute Error: UPDATE users SET password=password(?) WHERE username=?;";
   return FALSE;
}

   return TRUE;

}

function delete_post($postid){
global $mysqli;
    $prepared_sql = "delete from posts where id =?;";
  
  if(!$stmt = $mysqli->prepare($prepared_sql))
   echo "Prepared Statement Error";
   $stmt->bind_param("i",$postid);
  if(!$stmt->execute()) {
   echo "Execute Error: DELETE from posts where id =?;";
   return FALSE;
}

   return TRUE;
}

function insertinguserintodatadbase($newusername, $newpassword, $name, $email, $telephone) {
    global $mysqli;
    $prepared_sql = "INSERT into regularuser(username,password,name,email,telephone) VALUES (?,password(?),?,?,?);";
    if(!$stmt = $mysqli->prepare($prepared_sql))
	echo "Prepared Statement Error";
    $stmt->bind_param("ssssi", htmlspecialchars($newusername),htmlspecialchars($newpassword),htmlspecialchars($name),htmlspecialchars($email),htmlspecialchars($telephone));
    if(!$stmt->execute()) {echo "Execute Error"; return FALSE;}
    return TRUE;
}
  

function show_register_user() {
  //debug: echo "->show_posts: retreive the posts from database and display<br>";    
	
	

    global $mysqli;
   
	//echo "from posts ";

    $sql = "SELECT * FROM regularuser";
	
    $result = $mysqli->query($sql);
   
    if($result->num_rows > 0){

		while($row = $result->fetch_assoc()){
		
			$username = $row["username"];
	$approved = $row["approved"];
	$enabled = $row["enable"];
			//echo "<h3>Post " . $postid . " - " . $row["title"]. " 		</h3>";
			//echo $row["username"]."<br>";
			//echo " hi";
	echo '<table align="center" >';
	echo "<form action='approved.php?username=".$username."' method='POST'>";
	echo '<th>Username</th>';
	echo '<th>email</th>';
	echo '<th>Approval/Disapprove</th>';
	echo '<th>Enable/Disable</th>';
        echo '<tr>';
        echo '<td>';
        echo "<h3>" . $row["username"] . "</h3>";
        echo '</td>';
	echo '<td>';
        echo "<h3>" . $row["email"] . "</h3>";
        echo '</td>';
        echo '<td>';
	echo '<input type="radio" name ="approved" value=1 ' ;if($approved==1){echo"checked";}echo'/>Approve<br>';
	echo '<input type="radio" name ="approved" value=0 ';if($approved==0){echo"checked";}echo'/>Disapprove';
        echo '</td>';
        echo '<td>';
	echo '<input type="radio" name ="enabled" value=1 ';if($enabled==1){echo"checked";}echo'/>Enable<br>';
	echo '<input type="radio" name ="enabled" value=0 ';if($enabled==0){echo"checked";}echo'/>Disabled';
        echo '</td>';
        
	echo '<td>';
	echo '<input type="submit" name ="submit" value="submit"/>';
        echo '</td>';
	echo '</tr>';
        echo '</table>';
	echo '</form>';	
	 
        }
	
    }
	
}


function show_user_posts($user) {
global $mysqli;

$prepared_sql = "select id, title, text from posts where owner=?;";
if(!$stmt = $mysqli->prepare($prepared_sql)) echo "prepare failed";
$stmt->bind_param('s', htmlspecialchars($user));
if(!$stmt->execute()) echo "Execute failed";
$postid = NULL;
$title = NULL;
$text = NULL;
if(!$stmt->bind_result($postid, $title, $text)) echo "Binding failed";
$num_rows = 0;
echo"<table border='1'>";
while($stmt->fetch()){
echo"<tr>";
   //echo"<td>Post " . htmlentities($title). "</td>";
   echo "<td>".htmlentities($title)."</td>";
   echo"<td><a href='edit.php?postid=$postid'>edit</a></td>";
   echo"<td><a href='delete.php?postid=$postid'>delete</a></td>";
  echo"</tr>";

$num_rows++;
}

echo"</table>";
if($num_rows==0)echo "No Posts Exist, write a new post :) </a>";
}


/*
function approved($username,$approved) 
{
	echo "Shivasai";
    global $mysqli;
    $prepared_sql = "update regularuser set approved=? where username=? ;";
   
    if(!$stmt = $mysqli->prepare($prepared_sql))
	 echo "Prepared Statement Error";
    $stmt->bind_param("is",$approved, $username);
    if(!$stmt->execute()) 
	{
		
		echo "Execute Error"; 
		return FALSE;
		
	}
    return TRUE;
 }


function enabled($username,$disabled) { 


    echo "Shivasai";
    global $mysqli;
    $prepared_sql = "update regularuser set enable=? where username=? ;";
   
    if(!$stmt = $mysqli->prepare($prepared_sql))
	 echo "Prepared Statement Error";
    $stmt->bind_param("is",$disabled, $username);
    if(!$stmt->execute()) 
	{
		
		echo "Execute Error"; 
		return FALSE;
		
	}
    return TRUE;

}*/

   
?>






